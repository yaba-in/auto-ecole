import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudiantsNoninscrisListComponent } from './etudiants-noninscris-list.component';

describe('EtudiantsNoninscrisListComponent', () => {
  let component: EtudiantsNoninscrisListComponent;
  let fixture: ComponentFixture<EtudiantsNoninscrisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtudiantsNoninscrisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantsNoninscrisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
