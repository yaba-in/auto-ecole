import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudiantsInscrisListComponent } from './etudiants-inscris-list.component';

describe('EtudiantsInscrisListComponent', () => {
  let component: EtudiantsInscrisListComponent;
  let fixture: ComponentFixture<EtudiantsInscrisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtudiantsInscrisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantsInscrisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
