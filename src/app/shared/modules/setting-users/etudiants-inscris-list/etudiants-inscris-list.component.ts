import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AutoEcoleEtudiant } from 'src/app/entities/accounts';

@Component({
  selector: 'app-etudiants-inscris-list',
  templateUrl: './etudiants-inscris-list.component.html',
  styleUrls: ['./etudiants-inscris-list.component.scss']
})
export class EtudiantsInscrisListComponent implements OnInit, AfterViewInit {
  
displayedColumns: string[] = ['select','Nom & Prenom', 'Tel', 'Email'];
etudiants = generateDataTest();
dataSource:MatTableDataSource<any>=null;
@ViewChild(MatPaginator) paginator: MatPaginator;
@ViewChild(MatSort) sort: MatSort;
selection = new SelectionModel<any>(true, []);
showMultiUserActionBtn=false;

constructor() {
  // Create 100 users
  
}
ngOnInit(): void {
  this.dataSource = new MatTableDataSource<any>(this.etudiants)
}

ngAfterViewInit(): void {
  //this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
  this.selection.changed.subscribe((value)=>{
    this.showMultiUserActionBtn=this.selection.hasValue();
  })
}
 
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}

/** Whether the number of selected elements matches the total number of rows. */
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.displayedColumns.length;
  return numSelected === numRows;
}
 /** Selects all rows if they are not all selected; otherwise clear selection. */
 masterToggle() {
  this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
    
}
 /** The label for the checkbox on the passed row */
 checkboxLabel(row?: any): string {
  if (!row) {
    return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
  }
  return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row `;
}
}

function generateDataTest():AutoEcoleEtudiant[]
{
let e1=new AutoEcoleEtudiant();
e1.nom="Nguendap";
e1.prenom="Cédric";
e1.age=22;
e1.email="cednguendap@gmail.com";
e1.tel="+237 698 29 53 68";

let e2=new AutoEcoleEtudiant();
e2.nom="Nguendap";
e2.prenom="Sévérin";
e2.age=43;
e2.email="snguendap@gmail.com"
e2.tel="+237 677 11 39 45";

return [e1,e2];
}
