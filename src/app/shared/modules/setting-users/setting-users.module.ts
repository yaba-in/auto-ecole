import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingUsersComponent } from './setting-users/setting-users.component';
import { FormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule} from '@angular/material/input';
import { EtudiantsListComponent } from './etudiants-list/etudiants-list.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import { EtudiantsInscrisListComponent } from './etudiants-inscris-list/etudiants-inscris-list.component';
import { EtudiantsNoninscrisListComponent } from './etudiants-noninscris-list/etudiants-noninscris-list.component';
import { SettingEtudiantsModule } from '../setting-etudiants/setting-etudiants.module';
import { TableModule } from 'src/app/shared/components/table/table.module';



@NgModule({
  declarations: [
    SettingUsersComponent,
    EtudiantsListComponent,
    EtudiantsInscrisListComponent,
    EtudiantsNoninscrisListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    TableModule,
    MatIconModule,
    SettingEtudiantsModule
  ],
  exports:[
    SettingUsersComponent,
    EtudiantsListComponent
  ]
})
export class SettingUsersModule { }
