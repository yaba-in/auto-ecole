import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalConfirmInscriptionComponent } from './modal-confirm-inscription/modal-confirm-inscription.component';
import { BtnGestionEtudiantsComponent } from './btn-gestion-etudiants/btn-gestion-etudiants.component';
import {MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalAddStudentComponent } from './modal-add-student/modal-add-student.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTypeNumberModule } from '../../components/input-type-number/input-type-number.module';
import { CustomModalModule } from '../../components/modal/modal.module';
import { ModalDesactiveEtudiantsComponent } from './modal-desactive-etudiants/modal-desactive-etudiants.component';
import { ModalDeleteEtudiantsComponent } from './modal-delete-etudiants/modal-delete-etudiants.component';

@NgModule({
  declarations: [
    ModalConfirmInscriptionComponent,
    BtnGestionEtudiantsComponent,
    ModalAddStudentComponent,
    ModalDesactiveEtudiantsComponent,
    ModalDeleteEtudiantsComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    ModalModule.forRoot(),
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    InputTypeNumberModule,
    CustomModalModule
  ],
  exports:[
    ModalConfirmInscriptionComponent,
    BtnGestionEtudiantsComponent
  ]
})
export class SettingEtudiantsModule { }
