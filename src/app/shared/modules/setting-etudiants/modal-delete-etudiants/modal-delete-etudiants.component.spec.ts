import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteEtudiantsComponent } from './modal-delete-etudiants.component';

describe('ModalDeleteEtudiantsComponent', () => {
  let component: ModalDeleteEtudiantsComponent;
  let fixture: ComponentFixture<ModalDeleteEtudiantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDeleteEtudiantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteEtudiantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
