import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmInscriptionComponent } from './modal-confirm-inscription.component';

describe('ModalConfirmInscriptionComponent', () => {
  let component: ModalConfirmInscriptionComponent;
  let fixture: ComponentFixture<ModalConfirmInscriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalConfirmInscriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmInscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
