import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDesactiveEtudiantsComponent } from './modal-desactive-etudiants.component';

describe('ModalDesactiveEtudiantsComponent', () => {
  let component: ModalDesactiveEtudiantsComponent;
  let fixture: ComponentFixture<ModalDesactiveEtudiantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDesactiveEtudiantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDesactiveEtudiantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
