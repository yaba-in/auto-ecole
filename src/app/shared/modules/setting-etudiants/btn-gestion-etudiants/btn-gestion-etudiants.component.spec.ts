import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnGestionEtudiantsComponent } from './btn-gestion-etudiants.component';

describe('BtnGestionEtudiantsComponent', () => {
  let component: BtnGestionEtudiantsComponent;
  let fixture: ComponentFixture<BtnGestionEtudiantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtnGestionEtudiantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnGestionEtudiantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
