import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CreateAdminAccountComponent } from 'src/app/pages/started/create-admin-account/create-admin-account.component';
import { ModalAddStudentComponent } from '../modal-add-student/modal-add-student.component';
import { ModalConfirmInscriptionComponent } from '../modal-confirm-inscription/modal-confirm-inscription.component';
import { ModalDeleteEtudiantsComponent } from '../modal-delete-etudiants/modal-delete-etudiants.component';
import { ModalDesactiveEtudiantsComponent } from '../modal-desactive-etudiants/modal-desactive-etudiants.component';

@Component({
  selector: 'app-btn-gestion-etudiants',
  templateUrl: './btn-gestion-etudiants.component.html',
  styleUrls: ['./btn-gestion-etudiants.component.scss'],
  animations:[
    trigger('hideShow',[
      state('hide',style({
        // visibility: 'hidden',
        display:"none",
        opacity:0,
        marginLeft:"100px"
      })),
      state('show',style({
        // visibility:"visible",
        display:"block",
        marginLeft:"0px",
        opacity:1
      })),
      transition('show => hide',[
        animate('0.2s ease-out')
      ]),
      transition('hide => show',[
        animate('0.5s')
      ])
    ])
  ]
})
export class BtnGestionEtudiantsComponent implements OnInit {
  @Input() showMultiUserActionBtn:boolean=false;
  constructor(private modalService:BsModalService,private dialogService:MatDialog) { }

  ngOnInit(): void {
  }
  showInscriptionUI()
  {
    this.dialogService.open(ModalConfirmInscriptionComponent,{
      width:"60%"
    })
  }

  showDeactivationUI()
  {
    this.dialogService.open(ModalDesactiveEtudiantsComponent,{
      width:"60%"
    })
  }
  showDeleteAccountUI()
  {
    this.dialogService.open(ModalDeleteEtudiantsComponent,{
      width:"60%"
    })
  }
  showAddStudentUI()
  {
    // this.modalService.show(ModalAddStudentComponent,
    //   {
    //     class:"large"
    //   })
    this.dialogService.open(ModalAddStudentComponent,{
      width:"60%"
    })
  }

}
