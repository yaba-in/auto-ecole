import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from 'src/app/shared/utils/helpers/validators';

@Component({
  selector: 'app-modal-add-student',
  templateUrl: './modal-add-student.component.html',
  styleUrls: ['./modal-add-student.component.scss']
})
export class ModalAddStudentComponent implements OnInit {

  
  form:FormGroup;
  waitSubmittedForm=false;
  submitedForm=false;
  

  constructor(
    private formBuilder:FormBuilder,
    private router:Router) { }
  
  ngOnInit(): void {
    this.form=this.formBuilder.group({
      nom_etudiant:new FormControl("",[Validators.required,Validators.minLength(2)]),
      prenom_etudiant:new FormControl("",[Validators.required,Validators.minLength(6)]),
      email_etudiant: new FormControl("",[Validators.required,Validators.pattern('^[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z0-9]{2,6}')]),
      password_etudiant:new FormControl("", [Validators.required,Validators.minLength(8)]),
      tel_etudiant: new FormControl("",[Validators.required, Validators.minLength(5)]),
    })
  }

  submitForm()
  {
    this.submitedForm=true;
    // console.log("Value form ",this.form.value)
    if(this.form.invalid) return;

    // this.createAutoEcole.autoEcoleAdminAccount.nom=this.form.value.nom_admin;
    // this.createAutoEcole.autoEcoleAdminAccount.prenom=this.form.value.prenom_admin;
    // this.createAutoEcole.autoEcoleAdminAccount.email = this.form.value.email_admin;
    // this.createAutoEcole.autoEcoleAdminAccount.mdp=this.form.value.password_admin;
    // this.createAutoEcole.autoEcoleAdminAccount.tel=this.form.value.tel_admin.internationalNumber;
    this.router.navigate(["/start/choice-pricing-plan"]);
  }

  closeModal()
  {

  }
}
