import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomTableComponent } from './custom-table/custom-table.component';
import { SimpleTableComponent } from './simple-table/simple-table.component';
import { SelectionTableComponent } from './selection-table/selection-table.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';



@NgModule({
  declarations: [
    CustomTableComponent,
    SimpleTableComponent,
    SelectionTableComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
  ],
  exports:[
    CustomTableComponent,
    SimpleTableComponent,
    SelectionTableComponent]
})
export class TableModule { }
