import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.scss']
})
export class SimpleTableComponent implements OnInit, AfterViewInit {
  @Input() columnHeaders:String[]=[];
  @Input() columnContent:String[]=[];
  @Input() dataSource:any[]=[];
  @Input() matDataSource:MatTableDataSource<any>=null;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() { }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }


  ngAfterViewInit(): void {
    
  }

  eval(value:string,row:any)
  {
    return eval(value);
  }
}
