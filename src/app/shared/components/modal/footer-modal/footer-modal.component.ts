import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-footer-modal',
  templateUrl: './footer-modal.component.html',
  styleUrls: ['./footer-modal.component.scss']
})
export class FooterModalComponent implements OnInit {

  @Input() textValider="Valider";
  @Input() textAnnuler="Annuler";
  @Output() eventAnnuler:EventEmitter<any>=new EventEmitter()
  @Output() eventValider:EventEmitter<any>=new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }

  closeModal()
  {
    this.eventAnnuler.emit();
  }

  validModal()
  {
    this.eventValider.emit();
  }

}
