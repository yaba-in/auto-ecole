import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterModalComponent } from './footer-modal/footer-modal.component';
import { HeaderModalComponent } from './header-modal/header-modal.component';



@NgModule({
  declarations: [
    FooterModalComponent,
    HeaderModalComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    FooterModalComponent,
    HeaderModalComponent
  ]
})
export class CustomModalModule { }
