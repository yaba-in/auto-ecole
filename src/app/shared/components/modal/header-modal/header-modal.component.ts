import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-modal',
  templateUrl: './header-modal.component.html',
  styleUrls: ['./header-modal.component.scss']
})
export class HeaderModalComponent implements OnInit {
  @Input() text="Untitle";
  @Input() bgColor="#3b7ddd";
  @Input() icon="";
  constructor() { }

  ngOnInit(): void {
  }

}
